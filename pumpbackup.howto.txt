pumpbackup.sh how-to
========================

* Open your web browser and go to the pump.io server where your account is.
* Log in
* Get the name of the cookie that the browser uses (in Iceweasel, go to Edit >
Preferences > Privacy > Remove cookies individually, search for the cookie,
and copy the "content")
* Paste the content in the corresponding setting in the script (Warning! 2 places: line 9 (variable, not working yet) and line 23 (the wget command)
* Complete the rest of the settings with your info: pumpserver, your account, the folders where backups will be stored.
* You can learn the total number of messages (people you follow) going to 

https://yourPumpServer/api/user/yourUsername/feed 

and go to the bottom of that page, and look for the string "totalItems:"

* When you are done, save and close.
* Create the folder to store the backup, if it does not exist yet.
* Make the script executable. In your terminal:

$ chmod +x ./pumpbackup.sh

* Run the script from the commandline:

$ ./pumpbackup.sh

You'll get, in your backup folder, a compressed file with a JSON file for each 200 activities of your feed (major+minor).

I suggest to use the JSONView Firefox (and derivatives) add-on to view the JSON files coloured and indented in your web browser.