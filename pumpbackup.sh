# (c) 2014 Laura Arjona Reina (larjona@larjona.net)
# This script is in Public Domain
# Use at your own risk

#!/bin/bash

## EDIT OPTIONS ##
PB_I=0
PB_TOTALMESSAGES=3404
PB_COUNT=200
PB_SERVER=identi.ca
PB_USER=larjona
PB_COOKIE='Y89b____REDACTED____2B4bMlsk'
PB_WORKINGDIR=/home/larjona/Documentos/backups/pump
PB_DTE=$(date +%Y%m%d)

## END EDIT OPTIONS ##

cd $PB_WORKINGDIR
mkdir $PB_DTE
cd $PB_DTE

while [ $PB_I -le $PB_TOTALMESSAGES ]
do
	echo "wgetting $PB_COUNT items beginning with number $PB_I..." 
	echo $PB_COOKIE
	wget -O "$PB_I.json" --no-cookies --header 'Cookie: connect.sid=Y89b____REDACTED____2B4bMlsk' "https://$PB_SERVER/api/user/$PB_USER/feed?count=$PB_COUNT&offset=$PB_I"
	PB_I=`expr $PB_I + $PB_COUNT`
	echo $PB_I
	#sleep 3
done

cd ..
tar zcf ../pumpbackup_$PB_DTE.tar.gz $PB_DTE/
rm $PB_DTE/*.json
rmdir $PB_DTE

exit 0

