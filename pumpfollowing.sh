# (c) 2014 Laura Arjona Reina (larjona@larjona.net)
# This script is in Public Domain
# Use at your own risk

#!/bin/bash

## EDIT OPTIONS ##
PF_I=0
PF_TOTALMESSAGES=810
PF_COUNT=1
PF_SERVER=pumprock.net
PF_USER=poetas
PF_COOKIE= 'XXXXYYYYZZZ'
PF_WORKINGDIR=/home/username/backups/pumpfollowing
PF_DTE=$(date +%Y%m%d)

## END EDIT OPTIONS ##

cd $PF_WORKINGDIR
touch $PF_SERVER_$PF_USER_following_$PF_DTE.txt
mkdir $PF_DTE
cd $PF_DTE


while [ $i -le $PF_TOTALMESSAGES ]
do
	echo "wgetting $PF_COUNT items beginning with number $i..." 
	echo $PF_COOKIE
	wget -O "$PF_I.json" --no-cookies --header 'cookie: connect.sid=XXXXYYYYZZZZ' "https://$PF_SERVER/api/user/$PF_USER/following?count=$PF_COUNT&offset=$PF_I"

	echo "extracting name of account for txt listing"
	cat $PF_I.json | tr ',' '\n' | grep acct -m 1 >> $PF_SERVER_$PF_USER_following_$PF_DTE.txt
	
	PF_I=`expr $PF_I + $PF_COUNT`
	echo $PF_I
	#sleep 3
done

cd ..
tar zcf ../$PF_SERVER_$PF_USER_following_$PF_DTE.tar.gz $PF_DTE/
rm $PF_DTE/*.json
rm $PF_DTE/*.txt
rmdir $PF_DTE

exit 0
